from http.server import HTTPServer, BaseHTTPRequestHandler

HOST_ADDRESS = "localhost"
HOST_PORT = 20999

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        with open("."+self.path,'r') as f:    #el punto es para que me busque en mi directorio
            content = f.read()
            self.wfile.write("GET request for {} {}".format(self.path,content).encode('utf-8'))     #con esto, cuando pongo en el navegador la IP:puerto + nombre del fichero, me escribe en la pantalla de la pag web 
                                                                                                    #lo que haya en el fichero, que previamente debo haber leido (f.read())

    def do_POST(self):
        #content_length = int(self.headers['Content-Length'])
        #post_data = self.rfile.read(content_length)
        #print(post_data)
        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
