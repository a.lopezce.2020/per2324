from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "localhost"
HOST_PORT = 21999

formhtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<form action="/dame_respuesta">
  <label for="food">Alimento:</label><br>
  <input type="text" id="Alimento" name="Alimento" value="Pan"><br>
  <label for="nutr">Nutriente:</label><br>
  <input type="text" id="Nutriente" name="Nutriente" value="Carbohidrato"><br><br>
  <input type="submit" value="Submit">
</form> 

<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page.php".</p>

</body>
</html>
"""

# <input type="email" id="email" name="email"><br><br>   Este es tipo email, hasta que no metas un email no deja hacer el submit
# <input type="number" min=1 id="Numero" name="Numero" ><br><br> Este tipo numero solo permite poner numeros enteros

responsehtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<p> Tu alimento es: {} y el nutriente es: {} </p>
La url utilizada es {}

</body>
</html>"""

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"):
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/dame_respuesta")):          #empieza con
            query = urlparse(self.path).query                    #coge el path y quedate solo con los parametros (query --> lo de a partir de la interrogacion)
            query_components = dict(qc.split("=") for qc in query.split("&"))          #Lo que hace es trocear y crear un diccionario
            alimento = query_components["Alimento"]       #En esta linea y la de abajo poner el nombre "fname" y "lname" que proceda        
            nutriente = query_components["Nutriente"]
            print(alimento, nutriente)
            response = responsehtml.format(alimento,nutriente,self.path)
            self.wfile.write(response.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        self.wfile.write("POST data is {}".format(post_data ).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
